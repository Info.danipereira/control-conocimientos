'use strict';

// puntuaciones
const puntuaciones = [
  {
    equipo: 'Toros Negros',
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipo: 'Amanecer Dorado',
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipo: 'Águilas Plateadas',
    puntos: [5, 8, 3, 2, 5, 90],
  },
  {
    equipo: 'Leones Carmesí',
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipo: 'Rosas Azules',
    puntos: [2, 1, 3, 1, 4, 3, 9],
  },
  {
    equipo: 'Mantis Verdes',
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipo: 'Ciervos Celestes',
    puntos: [3, 1, 1, 1],
  },
  {
    equipo: 'Pavos Reales Coral',
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipo: 'Orcas Moradas',
    puntos: [2, 3, 3, 4, 7],
  },
];

/* const newArray = [];

const totalScore = puntuaciones.map((score) => {
  return newArray[score.puntos];
}); */

function getTeamRank(a) {
  const totalScore = a.map((score) => {
    const { puntos } = score;

    const reducedScore = puntos.reduce((acc, puntos) => acc + puntos);
    score.puntos = reducedScore;
    return score;
  });

  const comparedScore = (a, b) => {
    if (a.puntos < b.puntos) {
      return -1;
    }

    if (a.puntos > b.puntos) {
      return 1;
    }

    return 0;
  };

  const sortedScore = totalScore.sort(comparedScore);

  const finalScore1 = sortedScore[0];
  const finalScore2 = sortedScore[sortedScore.length - 1];

  return `The team with the Highest score is ${JSON.stringify(finalScore2)} \n
and team with the lowest score is ${JSON.stringify(finalScore1)}`;
}

console.log(getTeamRank(puntuaciones));
/* for (const score of puntuaciones) {
  console.log(score.puntos);
}
 */
