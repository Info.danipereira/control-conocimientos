'use strict';

/* - Generar una contraseña (número entero aleatorio del 0 al 100)
- Pedir al usuario que introduzca un número dentro de ese rango.
- Si el número introducido es igual a la contraseña, aparecerá en pantalla un mensaje indicando que ha ganado; si no, el mensaje indicará si la contraseña en un número mayor o menor al introducido y dará una nueva oportunidad.
- El usuario tendrá 5 oportunidades de acertar, si no lo consigue, aparecerá un mensaje indicando que ha perdido. */



const getNum = () => {
	const randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
	//Se que poniendo * 101 valdría pero de esta forma me aprendo la fórmula
	for (let i = 4; i >= 0; i--) {
		const userNum = +prompt(`Please, insert a number between 0 and 100.`);

		if (userNum > 100 || userNum < 0) {
			alert(
				'WRONG NUMBER. You need to insert a number between 0 and 100. (Both included)'
			); // Aviso al usuario si mete un numero que se pase de 100 y sea menor que 0 y no  se le restan intentos.
			i++;
		}

		if (randomNum === userNum) {
			alert(`YOU WIN. Password is ${userNum} `);
			break;
		}

		if (userNum < randomNum && userNum <= 100 && userNum >= 0 && i > 1) {
			alert(
				`Misssed! The number is higher than ${userNum}. you got ${i} tries left.`
			);
		} else if (userNum > randomNum && userNum <= 100 && userNum >= 0 && i > 1) {
			alert(
				`Misssed! The number is lower than ${userNum}. you got ${i} tries left.`
			);
		} else if (
			userNum < randomNum &&
			userNum <= 100 &&
			userNum >= 0 &&
			i === 1
		) {
			alert(
				`Misssed! The number is higher than ${userNum}. you got ${i} try left.`
			); // He creado estos alert para cuando sólo hay un intento , no se si merece la pena.
		} else if (
			userNum > randomNum &&
			userNum <= 100 &&
			userNum >= 0 &&
			i === 1
		) {
			alert(
				`Misssed! The number is lower than ${userNum}. you got ${i} try left.`
			);
		}

		if (i === 0) {
			alert(`YOU LOSE. The number was ${randomNum}`);
		}
	}
};
getNum();
