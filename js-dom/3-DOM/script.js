const body = document.body;
const divWatch = document.createElement('div');
const h1Watch = document.createElement('h1');
const h1 = document.querySelector('h1');

divWatch.appendChild(h1Watch);
body.appendChild(divWatch);

setInterval(() => {
  const currentDate = new Date();

  const editedNum = (a) => {
    if (a < 10) return `0${a}`;

    return a;
  };

  const hours = editedNum(currentDate.getHours());
  const minutes = editedNum(currentDate.getMinutes());
  const seconds = editedNum(currentDate.getSeconds());

  h1Watch.textContent = `${hours}:${minutes}:${seconds}`;
}, 1000);
